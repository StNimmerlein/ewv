package de.andrena.ausbildung.einwohnerverwaltung.services;

import org.joda.time.LocalDate;

import de.andrena.ausbildung.einwohnerverwaltung.beans.Address;
import de.andrena.ausbildung.einwohnerverwaltung.beans.Person;

public interface CitizenAdministration {

	void changeRegistration(Person citizen, Address address);

	Person createCitizen(String lastName, String firstName, LocalDate dateOfBirth);

}
