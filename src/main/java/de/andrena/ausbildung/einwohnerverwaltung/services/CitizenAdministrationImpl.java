package de.andrena.ausbildung.einwohnerverwaltung.services;

import org.joda.time.LocalDate;

import de.andrena.ausbildung.einwohnerverwaltung.beans.Address;
import de.andrena.ausbildung.einwohnerverwaltung.beans.City;
import de.andrena.ausbildung.einwohnerverwaltung.beans.Person;
import de.andrena.ausbildung.einwohnerverwaltung.utils.IdGenerator;

public class CitizenAdministrationImpl implements CitizenAdministration {

	private static CitizenAdministration instance;

	private CitizenAdministrationImpl() {
	}

	public static CitizenAdministration instance() {
		if (instance == null) {
			instance = new CitizenAdministrationImpl();
		}
		return instance;
	}

	@Override
	public Person createCitizen(String lastName, String firstName, LocalDate dateOfBirth) {
		Person person = new Person(IdGenerator.getNextId(), lastName, firstName, dateOfBirth);
		return person;
	}

	@Override
	public void changeRegistration(Person citizen, Address address) {
		long cityId = citizen.getAddress().getCityId();
		City city = CityServiceImpl.instance().getById(cityId);
		if (city != null) {
			city.removeCitizen(citizen);
		}
		citizen.setAddress(address);
		CityServiceImpl.instance().getById(address.getCityId()).addCitizen(citizen);
	}
}
