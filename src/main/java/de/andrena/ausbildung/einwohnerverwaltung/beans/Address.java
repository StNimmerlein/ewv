package de.andrena.ausbildung.einwohnerverwaltung.beans;

import java.io.Serializable;

import de.andrena.ausbildung.einwohnerverwaltung.services.CityServiceImpl;

public class Address implements Serializable {
	private static final long serialVersionUID = -4865575576508793155L;

	// private City city;
	private long cityId;
	private String streetName;
	private int houseNumber;
	private String houseNumberSupplement = "";
	private String zipCode;

	public static final Address NO_ADDRESS = new Address(City.NO_CITY, "00000", "NO STREET", 0);

	public Address(City city, String zipCode, String streetName, int houseNumber) {
		// this.city = city;
		this.cityId = city.getId();
		this.zipCode = zipCode;
		this.streetName = streetName;
		this.houseNumber = houseNumber;
	}

	public Address(City city, String zipCode, String streetName, int houseNumber, String houseNumberSupplement) {
		this(city, zipCode, streetName, houseNumber);
		this.houseNumberSupplement = houseNumberSupplement;
	}

	public String getStreetName() {
		return streetName;
	}

	// public City getCity() {
	// return CityServiceImpl.instance().getById(cityId);
	// }

	public long getCityId() {
		return cityId;
	}

	public int getHouseNumber() {
		return houseNumber;
	}

	public String getHouseNumberSupplement() {
		return houseNumberSupplement;
	}

	public String getZipCode() {
		return zipCode;
	}

	public boolean isSameAddress(Address other) {
		if (this == NO_ADDRESS) {
			return false;
		}
		return equals(other);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) cityId;
		result = prime * result + houseNumber;
		result = prime * result + houseNumberSupplement.hashCode();
		result = prime * result + streetName.hashCode();
		result = prime * result + zipCode.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Address other = (Address) obj;
		if (hashCode() != other.hashCode()) {
			return false;
		}
		if (cityId != other.cityId) {
			return false;
		}
		if (houseNumber != other.houseNumber) {
			return false;
		}
		if (!houseNumberSupplement.equals(other.houseNumberSupplement)) {
			return false;
		}
		if (!streetName.equals(other.streetName)) {
			return false;
		}
		if (!zipCode.equals(other.zipCode)) {
			return false;
		}
		return true;
	}

	public String asString() {
		if (this == NO_ADDRESS) {
			return "Adresse unbekannt";
		}
		return streetName + " " + houseNumber + houseNumberSupplement + ", " + zipCode + " " + CityServiceImpl.instance().getById(cityId).getName();
	}

	@Override
	public String toString() {
		return "Address [cityId=" + cityId + ", streetName=" + streetName + ", houseNumber=" + houseNumber + ", houseNumberSupplement=" + houseNumberSupplement + ", zipCode="
				+ zipCode + "]";
	}

}
