package de.andrena.ausbildung.einwohnerverwaltung.beans;

import java.io.Serializable;

import org.joda.time.LocalDate;

public class Person implements Serializable, Citizen {
	private static final long serialVersionUID = -8760141953299234732L;

	private long id;
	private String lastName;
	private String firstName;
	private LocalDate dateOfBirth;
	private Address address;
	private String phoneNumber;
	private String emailAddress;

	public Person(long id, String lastName, String firstName, LocalDate dateOfBirth) {
		this.id = id;
		this.lastName = lastName;
		this.firstName = firstName;
		this.dateOfBirth = dateOfBirth;
		this.address = Address.NO_ADDRESS;

	}

	public long getId() {
		return id;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}

	public String getPostalAddress() {
		Address address = getAddress();
		return firstName + " " + lastName + ", " + address.asString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Person other = (Person) obj;
		if (id != other.id) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Person [id=" + id + ", lastName=" + lastName + ", firstName=" + firstName + ", dateOfBirth=" + dateOfBirth
				+ ", address=" + address + ", phoneNumber=" + phoneNumber + ", emailAddress=" + emailAddress + "]";
	}

	public boolean hasSameAddressAsPerson(Person person) {
		return this.getAddress().isSameAddress(person.getAddress());
	}

}
