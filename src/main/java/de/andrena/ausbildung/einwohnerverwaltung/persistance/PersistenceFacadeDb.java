package de.andrena.ausbildung.einwohnerverwaltung.persistance;

import java.util.List;

import de.andrena.ausbildung.einwohnerverwaltung.beans.City;

public class PersistenceFacadeDb implements PersistenceFacade {

	@Override
	public void save(List<City> cities) {
	}

	@Override
	public List<City> loadCities() {
		return null;
	}

}
