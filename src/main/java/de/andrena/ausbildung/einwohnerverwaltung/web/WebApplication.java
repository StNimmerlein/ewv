package de.andrena.ausbildung.einwohnerverwaltung.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import de.andrena.ausbildung.einwohnerverwaltung.services.CityService;
import de.andrena.ausbildung.einwohnerverwaltung.services.CityServiceImpl;

@SpringBootApplication
public class WebApplication {

	public static void main(String[] args) {
		CityService cityService = CityServiceImpl.instance();
		SpringApplication.run(WebApplication.class, args);
	}
}
