package de.andrena.ausbildung.einwohnerverwaltung.web;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.websocket.server.PathParam;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.andrena.ausbildung.einwohnerverwaltung.beans.Address;
import de.andrena.ausbildung.einwohnerverwaltung.beans.Citizen;
import de.andrena.ausbildung.einwohnerverwaltung.beans.City;
import de.andrena.ausbildung.einwohnerverwaltung.beans.Person;
import de.andrena.ausbildung.einwohnerverwaltung.services.CitizenAdministration;
import de.andrena.ausbildung.einwohnerverwaltung.services.CitizenAdministrationImpl;
import de.andrena.ausbildung.einwohnerverwaltung.services.CityService;
import de.andrena.ausbildung.einwohnerverwaltung.services.CityServiceImpl;

@RestController
public class WebAccessController {
	private CityService cityService = CityServiceImpl.instance();
	private CitizenAdministration citizAdmin = CitizenAdministrationImpl.instance();

	@RequestMapping(value = "/createCity")
	public String createCity(@RequestParam(value = "name") String name) {
		cityService.createCity(name);
		return "<html><head><meta http-equiv=\"refresh\" content=\"0; url=cityCreated.html\" /></head></html>";
	}

	@RequestMapping("/createCitizen")
	public String createCitizen(@RequestParam(value = "firstName") String firstName, @RequestParam(value = "lastName") String lastName,
			@RequestParam(value = "dateOfBirth") String dateOfBirth, @RequestParam(value = "street") String street, @RequestParam(value = "houseNumber") int houseNumber,
			@RequestParam(value = "zipCode") String zipCode, @RequestParam(value = "cityId") long cityId) {
		City city = cityService.getById(cityId);
		if (city == null) {
			return "City does not exist!";
		}
		DateTimeFormatter formatter = DateTimeFormat.forPattern("dd.MM.yyyy");
		DateTime dob = formatter.parseDateTime(dateOfBirth);
		Address address = new Address(city, zipCode, street, houseNumber);
		Person person = citizAdmin.createCitizen(lastName, firstName, new LocalDate(dob));
		citizAdmin.changeRegistration(person, address);
		return "<html><head><meta http-equiv=\"refresh\" content=\"0; url=citizenCreated.html\" /></head></html>";
	}

	@RequestMapping("/showAllCities")
	public List<City> showAllCities() {
		return cityService.getAllCities();
	}

	@RequestMapping("/city/{cityId}")
	public City showCity(@PathVariable("cityId") Long id) {
		return cityService.getById(id);
	}

	@RequestMapping("/showCity/{cityId}/citizens")
	public Set<Citizen> showCitizensOfCity(@PathVariable("cityId") Long id) {
		return cityService.getById(id).getCitizens();
	}

	@RequestMapping("/showCity")
	public City showCityQuery(@RequestParam(name = "cityid") Long id) {
		return cityService.getById(id);
	}

	@RequestMapping("/citizen/{citizenId}")
	public Person getCitizen(@PathVariable("citizenId") Long id) {
		for (City city : cityService.getAllCities()) {
			for (Citizen citizen : city.getCitizens()) {
				Person person = (Person) citizen;
				if (id.equals(person.getId())) {
					return person;
				}
			}
		}
		return null;
	}

	@RequestMapping(value = "/citizen/{citizenId}/name", produces = "application/json")
	public String getCitizenName(@PathVariable("citizenId") Long id) {
		Person person = getCitizen(id);
		if (person == null) {
			return null;
		}
		return person.getFirstName() + " " + person.getLastName();
	}

	@RequestMapping(value = "/citizen/{citizenId}/address", produces = "application/json")
	public String getCitizenAddress(@PathVariable("citizenId") Long id) {
		Person person = getCitizen(id);
		if (person == null) {
			return null;
		}
		return person.getPostalAddress();
	}

	@RequestMapping("/buildExampleCities")
	public String buildExampleCity() {
		City muenchen = cityService.createCity("Muenchen");
		City strohwaldpuppendorf = cityService.createCity("Strohwaldpuppendorf");
		City neustadt = cityService.createCity("Neustadt");

		LocalDate date = new LocalDate(1337, 8, 15);
		Address am = new Address(muenchen, "12345", "Ligusterweg", 4);
		Address as = new Address(strohwaldpuppendorf, "12345", "Ligusterweg", 4);
		Address an = new Address(neustadt, "12345", "Ligusterweg", 4);

		citizAdmin.changeRegistration(citizAdmin.createCitizen("Klein", "Haenschen", date), am);
		citizAdmin.changeRegistration(citizAdmin.createCitizen("Karstenfrosch", "Guenther", date), am);
		citizAdmin.changeRegistration(citizAdmin.createCitizen("Puhmann", "Paule", date), am);
		citizAdmin.changeRegistration(citizAdmin.createCitizen("Werkel", "Willi", date), am);

		citizAdmin.changeRegistration(citizAdmin.createCitizen("Maffay", "Peter", date), as);
		citizAdmin.changeRegistration(citizAdmin.createCitizen("Petry", "Wolle", date), as);
		citizAdmin.changeRegistration(citizAdmin.createCitizen("Krause", "Micky", date), as);

		citizAdmin.changeRegistration(citizAdmin.createCitizen("Bluemchen", "Benjamin", date), an);
		citizAdmin.changeRegistration(citizAdmin.createCitizen("Kolumna", "Karla", date), an);
		citizAdmin.changeRegistration(citizAdmin.createCitizen("Karl", "Zoowaerter", date), an);
		citizAdmin.changeRegistration(citizAdmin.createCitizen("Gulliver", "Rabe", date), an);

		return "<html><body><h1>Beispielst&auml;dte erzeugt</h1><form action=\"index.html\"><input type=\"submit\" value=\"Zur&uuml;ck\" /></form></body></html>";
	}

	@RequestMapping("/showAllCitizens")
	public Set<Citizen> showAllCitizens() {
		Set<Citizen> allCitizens = new HashSet<>();
		for (City city : cityService.getAllCities()) {
			allCitizens.addAll(city.getCitizens());
		}
		return allCitizens;
	}

	@RequestMapping("/clearAll")
	public String cleatAll() {
		cityService.removeAllCities();
		return "<html><body><h1>St&auml;dte gel&ouml;scht</h1><form action=\"index.html\"><input type=\"submit\" value=\"Zur&uuml;ck\" /></form></body></html>";
	}

}
